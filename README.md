
<div align="center">

# Nie czuje jak gotuje
![Nasze logo](nieczujejakgotuje1.jpg)

## Nowoczesna strona poświęcona gotowaniu.

</div>

## Nie czuje jak gotuje - Wstęp

Dla programistów. Tutaj będzie znajdował się nasz projekt, który możemy edytować z dowolnego miejsca. Staramy się też nie udostępniać plików źródłowych tego projektu nikomu innemu. Kto jest za co odpowiedzialny, wyjaśni się w trakcie pracy nad projektem.
